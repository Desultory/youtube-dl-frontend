<?php
$DEBUG = 0;
ini_set('max_execution_time', 300);

//Ensure all required fields are set, otherwise exit
if( !isset($_POST['video'])     || empty($_POST['video'])     ||
    !isset($_POST['type'])      || empty($_POST['type'])      ||
    !isset($_POST['nameStyle']) || empty($_POST['nameStyle'])) {
	echo("POST Error");
	exit();
}


//video - URL or ID of video
//type - Video / Music / Subtitles
//nameStyle - Title / ID
//Declare settings, escape video url/ID as it will be passed to the command line
$video = escapeshellarg(trim($_POST['video']));
$type = $_POST['type'];
$nameStyle = $_POST['nameStyle'];

if( $DEBUG ) {
	$cmd = 'youtube-dl -v --rm-cache-dir --restrict-filenames --no-playlist';
} else {
	$cmd = 'youtube-dl -i -q --rm-cache-dir --no-warnings --restrict-filenames --no-playlist';
}

if( $nameStyle === "Title" ) {
	$baseName = get_video_title($video);
} elseif ( $nameStyle === "ID" ) {
	$baseName = get_video_id($video);
}

$regexr = '/[^A-Za-z0-9\-\._]/';

if( $type === "Video" ) {
	$name = preg_replace($regexr, '_', $baseName);
	$cmd = "$cmd -f 'bestvideo+bestaudio' --audio-quality 0 --audio-format best --merge-output-format mkv --xattrs -o '$name.%(ext)s'";
	$name = $name . '.mkv';
} elseif( $type === "Music" ) {
	$name = preg_replace($regexr, '_', $baseName);
	$cmd = "$cmd --extract-audio --embed-thumbnail --audio-format mp3 --audio-quality 0 -f 'bestaudio' -o '$name.%(ext)s'";
	$name = $name . '.mp3';
} elseif( $type === "Subtitles" ) {
	$name = preg_replace($regexr, '_', $baseName);
	$cmd = "$cmd --skip-download --write-auto-sub --sub-format srt -o '$name.%(ext)s'";
	$name = $name . '.srt';
}

$cmd = "$cmd $video";
shell_exec($cmd);
push_file($name);

function get_video_title( $url ) {
	$title = "youtube-dl --restrict-filenames --get-title $url";
	$title = trim(shell_exec($title));
	if( $title ) {
		return $title;
	} else {
		echo "Error getting video title";
		exit();
	}
}

function get_video_id( $url ) {
	$id = "youtube-dl --restrict-filenames --get-id $url";
	$id = trim(shell_exec($id));
	if( $id ) {
		return $id;
	} else {
		echo "Error getting video ID";
		exit();
	}
}

function push_file( $name ) {
	if( file_exists($name) ) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		header('Content-Type: ' . finfo_file($finfo, $name));
		$finfo = finfo_open(FILEINFO_MIME_ENCODING);
		header('Content-Transfer-Encoding: ' . finfo_file($finfo, $name));
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename=' . $name);
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($name));

		ob_clean();
		flush();
		readfile($name);
		unlink($name);
	} else {
		echo("Error, unable to download");
	}
}
